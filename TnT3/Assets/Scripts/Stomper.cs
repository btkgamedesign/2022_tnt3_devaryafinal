using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stomper : MonoBehaviour
{
    public int damageToDeal;//Deal damage to hurtbox

    private Rigidbody2D theRB2D;//Get Rigidbody for impulse
    public float bounceForce;//Bounce of enemy when dealt damage from above
    
    void Start()
    {
        theRB2D = transform.parent.GetComponent<Rigidbody2D>();//Gets Player Rigidbody
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "HurtBox")//If Enemy has Tag "HurtBox
        {
            other.gameObject.GetComponent<EnemyHP>().TakeDamage(damageToDeal);//Deal damage to enemy
            theRB2D.AddForce(transform.up * bounceForce, ForceMode2D.Impulse);//Bounce of enemy
        }
    }
}
