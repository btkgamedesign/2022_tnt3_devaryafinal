using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin2 : MonoBehaviour
{
        public int coinValue = 5;//shows value of coin

    private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))//If "Player" Tag is attached to player
        {
                ScoreManager.instance.ChangeScore(coinValue);//Change Score of coin when collected to 5
        }
        }
}
