using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin3 : MonoBehaviour
{
        public int coinValue = 10;//shows value of coin

    private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))//If "Player" Tag is attached to player
        {
                ScoreManager.instance.ChangeScore(coinValue);//Change Score of coin when collected to 10
        }
        }
}
