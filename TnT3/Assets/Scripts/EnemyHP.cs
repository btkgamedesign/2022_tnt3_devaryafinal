using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : MonoBehaviour
{
    public int enemyHP;//Shows enemy HP
    private int currentHP;//The current HP enemy holds
    // Start is called before the first frame update
    void Start()
    {
        currentHP = enemyHP; //Shows current HP as the Enemy HP
    }

    // Update is called once per frame
    void Update()
    {
        if(currentHP <= 0)//Checks if current HP is 0
        {
            Destroy(transform.parent.gameObject);//If current HP 0, Destroys Enemy Object
        }
    }

    public void TakeDamage(int damage)//TakeDamage from PLayer and reduce HP to 0
    {
        currentHP -= damage;
    }
}
