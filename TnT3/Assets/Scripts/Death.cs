using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour
{
    public int Respawn;//Respawns player to original spawn point
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player")) //If player has Tag "player", load current scene 
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}

